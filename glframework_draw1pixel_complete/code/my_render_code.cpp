#include <GL\glew.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <cstdio>
#include <cassert>
#include <SDL.h>


#include "GL_framework.h"

using namespace glm;

//DEFINICI�N DE VARIABLES GENERALES
float move = 0.0f;
float VariableFOV = 65.f;

///////// fw decl
namespace ImGui {
	void Render();
}

namespace Box {
	void setupCube();
	void cleanupCube();
	void drawCube();
}

namespace Cube {
	void setupCube();
	void cleanupCube();
	void updateCube(const glm::mat4& transform);
	void drawCube();
	void draw2Cubes(double);
	void drawSomeCubes();
}

////////////////


namespace RenderVars {

	float FOV = glm::radians(VariableFOV);
	const float zNear = 1.f;
	const float zFar = 50.f;

	//DEFINICI�N DE LAS PROJECCIONES
	glm::mat4 _projectionPerspective;
	glm::mat4 _projectionOrtho;
	glm::mat4 _projectionPerspective2;
	glm::mat4 _modelView;
	glm::mat4 _MVP;
	glm::mat4 _inv_modelview;
	glm::vec4 _cameraPoint;

	

	float panv[3] = { 0.f, -5.f, -15.f };
	float rota[2] = { 0.f, 0.f };
}
namespace RV = RenderVars;

void GLResize(int width, int height) {
	glViewport(0, 0, width, height);
	float scale = 50.0f;

	//Prespectiva sense modificar el FOV
	if (height != 0) RV::_projectionPerspective = glm::perspective(RV::FOV, (float)width / (float)height, RV::zNear, RV::zFar);
	else RV::_projectionPerspective = glm::perspective(RV::FOV, 0.f, RV::zNear, RV::zFar);

	//2� Prespectiva que utilitzarem per a modificar el FOV
	if (height != 0) RV::_projectionPerspective2 = glm::perspective(RV::FOV, (float)width / (float)height, RV::zNear, RV::zFar);
	else RV::_projectionPerspective2 = glm::perspective(RV::FOV, 0.f, RV::zNear, RV::zFar);

	//Prespectiva Orthonormal
	if (height != 0) RV::_projectionOrtho = glm::ortho(-(float)width / scale, (float)height / scale, -(float)width / scale, (float)height / scale, RV::zNear, RV::zFar);
	else RV::_projectionOrtho = glm::ortho(-(float)width / scale, (float)height / scale, -(float)width / scale, (float)height / scale, RV::zNear, RV::zFar);
}


void GLinit(int width, int height) {
	glViewport(0, 0, width, height);
	glClearColor(0.2f, 0.2f, 0.2f, 1.f);
	glClearDepth(1.f);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	//DEFINICI�N DE LAS PROJECCIONES: PRESPECTIVA Y ORTHONORMAL
	RV::_projectionPerspective = glm::perspective(RV::FOV, (float)width / (float)height, RV::zNear, RV::zFar);
	RV::_projectionOrtho = glm::ortho(-(float)width / 50.0f, (float)height / 50.0f, -(float)width / 50.0f, (float)height / 50.0f, RV::zNear, RV::zFar);
	
	// Setup shaders & geometry
	Box::setupCube();
	Cube::setupCube();

}

void GLrender(double currentTime, int tecla, int width, int height) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	move = sin(currentTime);

	RV::_modelView = glm::mat4(1.f);
	//Vista predefinida de la matriu sense apretar cap tecla 
	RV::_modelView = glm::translate(RV::_modelView, glm::vec3(RV::panv[0], RV::panv[1], RV::panv[2]));
	RV::_MVP = RV::_projectionPerspective * RV::_modelView;
	//Execuci� del exercici 1
	if (tecla == 1) {
		
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(-3.0f*move, 0.0, 0.0));
		RV::_MVP = RV::_projectionOrtho * RV::_modelView;
	} 
	//Execuci� del exercici 2
	if (tecla == 2) {

		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.0, 0.0, (-2.0f*move)/0.5));
		RV::_MVP = RV::_projectionPerspective * RV::_modelView;

	
	}
	//Execuci� del exercici 3
	if (tecla == 3) {

		float superFOV = 1 + 0.5* move;

		RV::_projectionPerspective2 = glm::perspective(superFOV, (float)width / (float)height, RV::zNear, RV::zFar);
		RV::_MVP = RV::_projectionPerspective2 * RV::_modelView;
	}
	//Execuci� del exercici 4
	if (tecla == 4) {
		
		RV::_modelView = glm::translate(RV::_modelView, glm::vec3(0.0f, 0.0, 1+(move/ 0.5)));
		float superFOV = 0.75 + 0.5* move;

		RV::_projectionPerspective2 = glm::perspective(superFOV, (float)width / (float)height, RV::zNear, RV::zFar);
				
		RV::_MVP = RV::_projectionPerspective2 * RV::_modelView;
	}

	// render code
		
	//Cube::drawCube();
	//Cube::draw2Cubes(currentTime);

	Box::drawCube();
	Cube::drawSomeCubes();

	ImGui::Render();
}

void GLcleanup() {

	Box::cleanupCube();
	Cube::cleanupCube();

}


//////////////////////////////////// COMPILE AND LINK
GLuint compileShader(const char* shaderStr, GLenum shaderType, const char* name = "") {
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderStr, NULL);
	glCompileShader(shader);
	GLint res;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &res);
	if (res == GL_FALSE) {
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetShaderInfoLog(shader, res, &res, buff);
		fprintf(stderr, "Error Shader %s: %s", name, buff);
		delete[] buff;
		glDeleteShader(shader);
		return 0;
	}
	return shader;
}
void linkProgram(GLuint program) {
	glLinkProgram(program);
	GLint res;
	glGetProgramiv(program, GL_LINK_STATUS, &res);
	if (res == GL_FALSE) {
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &res);
		char *buff = new char[res];
		glGetProgramInfoLog(program, res, &res, buff);
		fprintf(stderr, "Error Link: %s", buff);
		delete[] buff;
	}
}

////////////////////////////////////////////////// BOX
namespace Box {
	GLuint cubeVao;
	GLuint cubeVbo[2];
	GLuint cubeShaders[2];
	GLuint cubeProgram;

	float cubeVerts[] = {
		// -5,0,-5 -- 5, 10, 5
		-30.f,  0.f, -10.f,
		30.f,  0.f, -10.f,
		30.f,  0.f,  10.f,
		-30.f,  0.f,  10.f,
		-30.f, 10.f, -10.f,
		30.f, 10.f, -10.f,
		30.f, 10.f,  10.f,
		-30.f, 10.f,  10.f,
	};
	GLubyte cubeIdx[] = {
		1, 0, 2, 3, // Floor - TriangleStrip
		0, 1, 5, 4, // Wall - Lines
		1, 2, 6, 5, // Wall - Lines
		2, 3, 7, 6, // Wall - Lines
		3, 0, 4, 7  // Wall - Lines
	};

	const char* vertShader_xform =
		"#version 330\n\
in vec3 in_Position;\n\
uniform mat4 mvpMat;\n\
void main() {\n\
	gl_Position = mvpMat * vec4(in_Position, 1.0);\n\
}";
	const char* fragShader_flatColor =
		"#version 330\n\
out vec4 out_Color;\n\
uniform vec4 color;\n\
void main() {\n\
	out_Color = color;\n\
}";

	void setupCube() {
		glGenVertexArrays(1, &cubeVao);
		glBindVertexArray(cubeVao);
		glGenBuffers(2, cubeVbo);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 24, cubeVerts, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeVbo[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLubyte) * 20, cubeIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		cubeShaders[0] = compileShader(vertShader_xform, GL_VERTEX_SHADER, "cubeVert");
		cubeShaders[1] = compileShader(fragShader_flatColor, GL_FRAGMENT_SHADER, "cubeFrag");

		cubeProgram = glCreateProgram();
		glAttachShader(cubeProgram, cubeShaders[0]);
		glAttachShader(cubeProgram, cubeShaders[1]);
		glBindAttribLocation(cubeProgram, 0, "in_Position");
		linkProgram(cubeProgram);
	}
	void cleanupCube() {
		glDeleteBuffers(2, cubeVbo);
		glDeleteVertexArrays(1, &cubeVao);

		glDeleteProgram(cubeProgram);
		glDeleteShader(cubeShaders[0]);
		glDeleteShader(cubeShaders[1]);
	}
	void drawCube() {
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RV::_MVP));
		// FLOOR
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.6f, 0.6f, 0.6f, 1.f);
		glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, 0);
		// WALLS
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 0.f, 1.f);
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 4));
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 8));
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 12));
		glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_BYTE, (void*)(sizeof(GLubyte) * 16));

		glUseProgram(0);
		glBindVertexArray(0);
	}
}

////////////////////////////////////////////////// CUBE
namespace Cube {
	GLuint cubeVao;
	GLuint cubeVbo[3];
	GLuint cubeShaders[2];
	GLuint cubeProgram;
	glm::mat4 objMat = glm::mat4(1.f);

	extern const float halfW = 0.5f;
	int numVerts = 24 + 6; // 4 vertex/face * 6 faces + 6 PRIMITIVE RESTART

						   //   4---------7
						   //  /|        /|
						   // / |       / |
						   //5---------6  |
						   //|  0------|--3
						   //| /       | /
						   //|/        |/
						   //1---------2
	glm::vec3 verts[] = {
		glm::vec3(-halfW, -halfW, -halfW),
		glm::vec3(-halfW, -halfW,  halfW),
		glm::vec3(halfW, -halfW,  halfW),
		glm::vec3(halfW, -halfW, -halfW),
		glm::vec3(-halfW,  halfW, -halfW),
		glm::vec3(-halfW,  halfW,  halfW),
		glm::vec3(halfW,  halfW,  halfW),
		glm::vec3(halfW,  halfW, -halfW)
	};
	glm::vec3 norms[] = {
		glm::vec3(0.f, -1.f,  0.f),
		glm::vec3(0.f,  1.f,  0.f),
		glm::vec3(-1.f,  0.f,  0.f),
		glm::vec3(1.f,  0.f,  0.f),
		glm::vec3(0.f,  0.f, -1.f),
		glm::vec3(0.f,  0.f,  1.f)
	};

	glm::vec3 cubeVerts[] = {
		verts[1], verts[0], verts[2], verts[3],
		verts[5], verts[6], verts[4], verts[7],
		verts[1], verts[5], verts[0], verts[4],
		verts[2], verts[3], verts[6], verts[7],
		verts[0], verts[4], verts[3], verts[7],
		verts[1], verts[2], verts[5], verts[6]
	};
	glm::vec3 cubeNorms[] = {
		norms[0], norms[0], norms[0], norms[0],
		norms[1], norms[1], norms[1], norms[1],
		norms[2], norms[2], norms[2], norms[2],
		norms[3], norms[3], norms[3], norms[3],
		norms[4], norms[4], norms[4], norms[4],
		norms[5], norms[5], norms[5], norms[5]
	};
	GLubyte cubeIdx[] = {
		0, 1, 2, 3, UCHAR_MAX,
		4, 5, 6, 7, UCHAR_MAX,
		8, 9, 10, 11, UCHAR_MAX,
		12, 13, 14, 15, UCHAR_MAX,
		16, 17, 18, 19, UCHAR_MAX,
		20, 21, 22, 23, UCHAR_MAX
	};




	const char* cube_vertShader =
		"#version 330\n\
	in vec3 in_Position;\n\
	in vec3 in_Normal;\n\
	out vec4 vert_Normal;\n\
	uniform mat4 objMat;\n\
	uniform mat4 mv_Mat;\n\
	uniform mat4 mvpMat;\n\
	void main() {\n\
		gl_Position = mvpMat * objMat * vec4(in_Position, 1.0);\n\
		vert_Normal = mv_Mat * objMat * vec4(in_Normal, 0.0);\n\
	}";


	const char* cube_fragShader =
		"#version 330\n\
in vec4 vert_Normal;\n\
out vec4 out_Color;\n\
uniform mat4 mv_Mat;\n\
uniform vec4 color;\n\
void main() {\n\
	out_Color = vec4(color.xyz * dot(vert_Normal, mv_Mat*vec4(0.0, 1.0, 0.0, 0.0)) + color.xyz * 0.3, 1.0 );\n\
}";
	void setupCube() {
		glGenVertexArrays(1, &cubeVao);
		glBindVertexArray(cubeVao);
		glGenBuffers(3, cubeVbo);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[0]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVerts), cubeVerts, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNorms), cubeNorms, GL_STATIC_DRAW);
		glVertexAttribPointer((GLuint)1, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(1);

		glPrimitiveRestartIndex(UCHAR_MAX);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cubeVbo[2]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cubeIdx), cubeIdx, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		cubeShaders[0] = compileShader(cube_vertShader, GL_VERTEX_SHADER, "cubeVert");
		cubeShaders[1] = compileShader(cube_fragShader, GL_FRAGMENT_SHADER, "cubeFrag");

		cubeProgram = glCreateProgram();
		glAttachShader(cubeProgram, cubeShaders[0]);
		glAttachShader(cubeProgram, cubeShaders[1]);
		glBindAttribLocation(cubeProgram, 0, "in_Position");
		glBindAttribLocation(cubeProgram, 1, "in_Normal");
		linkProgram(cubeProgram);
	}
	void cleanupCube() {
		glDeleteBuffers(3, cubeVbo);
		glDeleteVertexArrays(1, &cubeVao);

		glDeleteProgram(cubeProgram);
		glDeleteShader(cubeShaders[0]);
		glDeleteShader(cubeShaders[1]);
	}
	void updateCube(const glm::mat4& transform) {
		objMat = transform;
	}
	void drawCube() {
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 0.f, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);
				 
		objMat = glm::translate(glm::mat4(), glm::vec3(1.0f, 2.0f, 1.0f));
		
		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);
	}
	
	void draw2Cubes(double currentime)
	{
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);

		glm::mat4 t = glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.0f, 3.0f));
		objMat = t;
		float red = 0.f;
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		float scal = 1.f + 0.5f *sin(3.f*currentime);
		glm::mat4 s = glm::scale(glm::mat4(), glm::vec3(scal));

		float rot = currentime;
		glm::mat4 r = glm::rotate(glm::mat4(), rot, glm::vec3(0.0f, 1.0, 0.0f));

		glm::mat4 tr = r * t;

		//float move = 1.f + 0.5f *sin(3.f*currentime);
		glm::mat4 t2 = glm::translate(glm::mat4(), glm::vec3(1.0f, 0.f,/*move,*/ 3.0f));

		objMat = t * r*t2*s;

		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		 red = 0.5f + 0.5f*sin(3.f*currentime);
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, red, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);

	}

	void drawSomeCubes(){
		glEnable(GL_PRIMITIVE_RESTART);
		glBindVertexArray(cubeVao);
		glUseProgram(cubeProgram);

		objMat = glm::translate(glm::mat4(), glm::vec3(-1.0f, 2.0f, 3.0f));
		
		//CUBO ROJO
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mv_Mat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_modelView));
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "mvpMat"), 1, GL_FALSE, glm::value_ptr(RenderVars::_MVP));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 0.f, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);


		objMat = glm::translate(glm::mat4(), glm::vec3(1.0f, 5.0f, 8.0f));
		
		//CUBO VERDE
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 1.f, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		objMat = glm::translate(glm::mat4(), glm::vec3(5.0f, 3.0f, -2.0f));

		//CUBO AZUL
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 1., 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		objMat = glm::translate(glm::mat4(), glm::vec3(-3.0f, 4.0f, 11.0f));

		//CUBO ROSA
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 0.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		objMat = glm::translate(glm::mat4(), glm::vec3(-4.0f, 7.0f, 1.0f));

		//CUBO BLANCO
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		//glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 0.f, 0.f);
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.f, 0.f, 0.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		objMat = glm::translate(glm::mat4(), glm::vec3(-2.0f, 4.0f, 6.0f));

		//CUBO NEGRO
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 1.f, 1.f, 1.f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		objMat = glm::translate(glm::mat4(), glm::vec3(0.0f, 5.0f, -7.0f));

		//CUBO AZUL/MORADO
		glUniformMatrix4fv(glGetUniformLocation(cubeProgram, "objMat"), 1, GL_FALSE, glm::value_ptr(objMat));
		glUniform4f(glGetUniformLocation(cubeProgram, "color"), 0.5f, 0.25f, 1.0f, 0.f);
		glDrawElements(GL_TRIANGLE_STRIP, numVerts, GL_UNSIGNED_BYTE, 0);

		glUseProgram(0);
		glBindVertexArray(0);
		glDisable(GL_PRIMITIVE_RESTART);

		
	}
	
}


/////////////////////////// "MY FIRST SHADER"  ////////////////////////
/*namespace MyFirstShader {

	// 1. Define the shader source code
	static const GLchar* vertex_shader_source[] =
	{
		"#version 330\n\
			\n\
			void main(){\n\
			const vec4 vertices[3] = vec4[3](vec4(0.25, -0.25, 0.5, 1.0),\n\
									 vec4(-0.25, -0.25, 0.5, 1.0),\n\
									 vec4(0.25, 0.25, 0.5, 1.0));\n\
					gl_Position = vertices[gl_VertexID];\n\
			}"
	};

	static const GLchar* fragment_shader_source[] =
	{
		"#version 330\n\
		\n\
		out vec4 color; \n\
		\n\
		void main() { \n\
		color = vec4(0.0,0.8,1.0,1.0);\n\}"
	};
	// 2. Compile and link the shaders

	GLuint MyShaderCompile(void) {
		GLuint vertex_shader;
		GLuint fragment_shader;
		GLuint program;

		vertex_shader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
		glCompileShader(vertex_shader);

		fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
		glCompileShader(fragment_shader);

		program = glCreateProgram();
		glAttachShader(program, vertex_shader);
		glAttachShader(program, fragment_shader);
		glLinkProgram(program);

		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);

		return program;

	}

	// 3. init function
	void MyInitCode(void) {

		MyRenderProgram = MyShaderCompile();
		glGenVertexArrays(1, &myVAO);
		glBindVertexArray(myVAO);

	}

	// 4. render function 
	void MyRenderCode(double currentTime) {
		const GLfloat red[] = { 0.5f * (float)sin(currentTime) + 0.5f,0.5f * (float)cos(currentTime) + 0.5f,0.0f,1.0f };
		glClearBufferfv(GL_COLOR, 0, red);

		glUseProgram(MyRenderProgram);
		glPointSize(40.0f);
		//glDrawArrays(GL_POINTS, 0, 3); //Pinta cubos
		//glDrawArrays(GL_LINE, 0, 3); //Pinta lineas
		//glDrawArrays(GL_LINE_LOOP, 0, 3); //Pinta 3 linies que formen un triangle
		glDrawArrays(GL_TRIANGLES, 0, 3); //Pinta 3 linies que formen un triangle

	}


	// 5. cleanup function
	void MyCleanupCode(void) {
		glDeleteVertexArrays(1, &myVAO);
		glDeleteProgram(MyRenderProgram);
	}

}*/